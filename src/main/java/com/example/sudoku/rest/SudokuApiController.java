package com.example.sudoku.rest;

import com.example.sudoku.dto.BoardDto;
import com.example.sudoku.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/sudoku")
public class SudokuApiController {

    private final SudokuService sudokuService;

    @Autowired
    public SudokuApiController(SudokuService sudokuService) {
        this.sudokuService = sudokuService;
    }

    @CrossOrigin
    @PostMapping(value = "/verify")
    public ResponseEntity<BoardDto>  verifySudoku(@RequestBody MultipartFile file){
        BoardDto board = sudokuService.verifyBoard(file);
        if(board.isValid()){
            return new ResponseEntity<>(board, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(board, HttpStatus.BAD_REQUEST);
        }
    }
}
