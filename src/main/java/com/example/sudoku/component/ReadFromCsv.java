package com.example.sudoku.component;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReadFromCsv {
    int[][] readCsv(MultipartFile file) throws IOException;
}
