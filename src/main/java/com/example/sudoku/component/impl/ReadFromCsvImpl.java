package com.example.sudoku.component.impl;
import com.example.sudoku.component.ReadFromCsv;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class ReadFromCsvImpl implements ReadFromCsv {
    public int[][] readCsv(MultipartFile file) throws IOException {
        int[][] board = new int[9][9];
        String[][] lines = new String[9][9];
        byte[] bytes = file.getBytes();
        ByteArrayInputStream inputFilestream = new ByteArrayInputStream(bytes);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputFilestream));
        String line = "";
        int li = 0;
        while ((line = br.readLine()) != null) {
            lines[li] = (line.split(","));
            li++;
        }
        br.close();
        for (int i = 0; i < 9; i++) {
            for (int k = 0; k < 9; k++) {
                board[i][k] = Integer.parseInt(lines[i][k]);
            }
        }
        return board;
    }
}
