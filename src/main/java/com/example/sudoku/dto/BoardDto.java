package com.example.sudoku.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class BoardDto {
    private List<Integer> lineIds = new ArrayList<>();
    private List<Integer> columnIds = new ArrayList<>();
    private List<Integer> areaIds = new ArrayList<>();
    private boolean isValid = true;

    public void addLine(int id) {
        this.lineIds.add(id);
    }

    public void addColumn(int id) {
        this.columnIds.add(id);
    }

    public void addArea(int id) {
        this.areaIds.add(id);
    }

    public void clearMistakes() {
        lineIds.clear();
        columnIds.clear();
        areaIds.clear();
    }

    public boolean containsMistakes() {
        return lineIds.isEmpty() && columnIds.isEmpty() && areaIds.isEmpty();
    }
}

