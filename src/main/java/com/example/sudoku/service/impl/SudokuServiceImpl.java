package com.example.sudoku.service.impl;
import com.example.sudoku.component.ReadFromCsv;
import com.example.sudoku.dto.BoardDto;
import com.example.sudoku.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.*;

@Service
public class SudokuServiceImpl implements SudokuService {

    @Autowired
    ReadFromCsv readFromCsv;
    private int[][] board = new int[9][9];
    private final BoardDto mistakes = new BoardDto();
    int size = 9;

    public BoardDto verifyBoard(MultipartFile file)  {
        mistakes.setValid(true);
        mistakes.clearMistakes();
        try {
            board = readFromCsv.readCsv(file);
        }catch (Exception e){
            mistakes.setValid(false);
            return mistakes;
        }
        isValidSudoku();
        mistakes.setValid(mistakes.containsMistakes());
        return mistakes;
    }

    private void isValidSudoku() {
        boolean[] unique = new boolean[size + 1];

        for (int i = 0; i < size; i++) {
            Arrays.fill(unique, false);
            for (int j = 0; j < size; j++) {
                int Z = board[i][j];
                // Check if current row
                // stores duplicate value
                if (unique[Z]) {
                    mistakes.addLine(i + 1);
                }
                unique[Z] = true;
            }
        }
        for (int i = 0; i < size; i++) {

            Arrays.fill(unique, false);

            for (int j = 0; j < size; j++) {

                int Z = board[j][i];
                // Check if current column
                // stores duplicate value
                if (unique[Z]) {
                    mistakes.addColumn(i + 1);
                }
                unique[Z] = true;
            }
        }
        int area = 0;
        for (int i = 0; i < size - 2; i += 3) {
            for (int j = 0; j < size - 2; j += 3) {
                area++;
                Arrays.fill(unique, false);
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {

                        int X = i + k;
                        int Y = j + l;
                        int Z = board[X][Y];

                        if (unique[Z]) {
                            mistakes.addArea(area);
                        }
                        unique[Z] = true;
                    }
                }
            }
        }
    }
}


