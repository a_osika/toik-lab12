package com.example.sudoku.service;
import com.example.sudoku.dto.BoardDto;
import org.springframework.web.multipart.MultipartFile;


public interface SudokuService {
    BoardDto verifyBoard(MultipartFile file);
}
